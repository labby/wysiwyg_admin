<?php

/**
 *	@module			wysiwyg Admin
 *	@version		see info.php of this module
 *	@authors		Dietrich Roland Pehlke
 * 	@copyright      2010-2022 Dietrich Roland Pehlke
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {
	include(LEPTON_PATH.'/framework/class.secure.php');
} else {
	$root = "../";
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= "../";
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) {
		include($root.'/framework/class.secure.php');
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

// get Instance of own class
$oWYSIWYG_ADMIN = wysiwyg_admin::getInstance();

$statusMessage = "";

$table = TABLE_PREFIX."mod_wysiwyg_admin";

/**
 *	New way to get information about the used editor.
 */
if (!defined("LEPTON_PATH") ) define("LEPTON_PATH", LEPTON_PATH);

$look_up = LEPTON_PATH."/modules/".WYSIWYG_EDITOR."/class.editorinfo.php";
if (file_exists($look_up))
{
	require_once( $look_up );
	if (!isset($editor_ref) || !is_object($editor_ref)) eval( "\$editor_ref = new editorinfo_".strtoupper(WYSIWYG_EDITOR)."();" );

} else {
	$statusMessage = array( "STATUS" => false, "MESSAGE_ID" => "SAVE_FAIL_DRIVER" );
	// Backwards compatible to 0.2.x
	require_once( dirname(__FILE__)."/driver/".WYSIWYG_EDITOR."/c_editor.php");
	if (!isset($editor_ref) || !is_object($editor_ref)) $editor_ref = new c_editor();
}

/**
 *	Something to save or delete?
 *
 */
if ( true === isset($_POST['job']) )
{
	if (( true === isset($_SESSION['wysiwyg_admin_hash']) 
	 && ( $_SESSION['wysiwyg_admin_hash'] == $_POST['hash']) ))
	{
		unset($_SESSION['wysiwyg_admin_hash']);
		unset($_POST['hash']);

		switch($_POST['job'])
		{
			case 'save':
				$values =  array_map('strip_tags', $_POST);
				$values['width'] = ($values['width'] == '') ? $editor_ref->default_width : $values['width'];
				$values['height'] = ($values['height'] == '') ? $editor_ref->default_height : $values['height'];

				$fields = array(
					'skin'	=> $values['skin'],
					'menu'	=> $values['menu'],
					'width' => intval($values['width'])."%",
					'height' => intval($values['height'])."px"
				);
	//die(LEPTON_tools::display($values, 'pre','ui blue message'));
				$database->build_and_execute(
					'update',
					$table,
					$fields,
					"id='".intval($values['id'])."'"
				);

				$statusMessage = array( "STATUS" => true, "MESSAGE_ID" => "SAVE_OK" );
				break;

			default:
				// nothing
		}
	} else {
		$statusMessage = array( "STATUS" => false, "MESSAGE_ID" => "SAVE_FAILED" );
	}
}

$data = array();
$database->execute_query (
	"SELECT id, skin, menu, height, width FROM ".$table." WHERE editor ='".WYSIWYG_EDITOR."' limit 0,1",
	true,
	$data,
	false
);

if (count($data) == 0) // !
{

	$lookup = LEPTON_PATH."/modules/".WYSIWYG_EDITOR."/class.editorinfo.php";
	if (file_exists($lookup))
	{
		require_once( $lookup );
		eval( "\$editor_info = new editorinfo_".strtoupper(WYSIWYG_EDITOR)."();" );
		$editor_info->wysiwyg_admin_init( $database );

		$last_insert_id = (true === $database->get_db_handle() instanceof PDO )
			? $database->get_db_handle()->lastInsertId()
			: $database->getOne("SELECT LAST_INSERT_ID()")
			;

		$toolbars = array_keys( $editor_info->toolbars );

		$data = array(
			'id'	=> $last_insert_id,
			'skin' => $editor_info->skins[0],
			'menu' => $toolbars[0],
			'width' => $editor_info->default_width,
			'height' => $editor_info->default_height
		);
	}
	else
	{
		// no editor-info avaible - so we have to use empty values
		$fields = array(
			'editor'=> WYSIWYG_EDITOR,
			'skin' 	=> '',
			'menu'	=> '',
			'width' => '100%',
			'height'=> '250px'
		);
		$database->build_and_execute(
		'INSERT',
		$table,
		$fields
		);

		$last_insert_id = (true === $database->db_handle instanceof PDO )
			? $database->get_db_handle()->lastInsertId()
			: $database->getOne("SELECT LAST_INSERT_ID()")
			;

		$data = array(
			'id'	=> $last_insert_id,
			'skin' => '',
			'menu' => '',
			'width' => '100%',
			'height' => '250px'
		);
	}
}

//  [0] build hash
$sHash = $oWYSIWYG_ADMIN->generateHash();
$_SESSION['wysiwyg_admin_hash'] = $sHash;

$leptoken = (isset($_GET['leptoken']) ? "?leptoken=".$_GET['leptoken'] : "" );

$templateValues = array(
	'ADMIN_URL'		=> ADMIN_URL,
	'oWYSIWYG_ADMIN'=> $oWYSIWYG_ADMIN,
	"hash"			=> $sHash,
	'time'			=> TIME(),
	'id'			=> $data['id'],
	'select_SKIN'	=> $editor_ref->build_select("skins", "skin", $data['skin']),
	'select_TOOL'	=> $editor_ref->build_select("toolbars", "menu", $data['menu']),
	'width'			=> (WYSIWYG_EDITOR == "tinymce") ? str_replace("%","", $data["width"]) : $data['width'],
	'height'		=> (WYSIWYG_EDITOR == "tinymce") ? str_replace("px","", $data["height"]) : $data['height'],
	'leptoken'		=> $leptoken
);
if ( true === is_array( $statusMessage ))
	{ $templateValues["message"] = $statusMessage; }

$oTWIG = lib_twig_box::getInstance();
$oTWIG->registerModule("wysiwyg_admin");

echo $oTWIG->render(
	 "@wysiwyg_admin/tool.lte", // template-filename
	$templateValues	//	template-data
);

// Preview section:
	$section_id = -1;
	$page_id = -120;
	$_GET['page_id'] = $page_id;
	$preview = true;

	global $id_list;
	$id_list= array( 1 );

	require_once(LEPTON_PATH."/modules/wysiwyg/modify.php");

	$section_id *= -1;

	show_wysiwyg_editor(
	    'content'.$section_id,
	    'content'.$section_id,
	    $content,
	    $templateValues["width"],
	    $templateValues["height"],
	    true
	);

