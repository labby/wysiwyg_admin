<?php

/**
 *	@module			wysiwyg Admin
 *	@version		see info.php of this module
 *	@authors		Dietrich Roland Pehlke
 * 	@copyright      2010-2022 Dietrich Roland Pehlke
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

  
LEPTON_handle::drop_table('mod_editor_admin');

$table_fields="
	`id`		int(11) NOT NULL AUTO_INCREMENT,
	`skin`		varchar(255) NOT NULL DEFAULT 'none',
	`menu`		varchar(255) NOT NULL DEFAULT 'none',
	`width`		varchar(64) NOT NULL DEFAULT '100',
	`height`	varchar(64) NOT NULL DEFAULT '400',
	`editor`	varchar(255) NOT NULL DEFAULT 'none',
	PRIMARY KEY (`id`)
";
LEPTON_handle::install_table('mod_wysiwyg_admin', $table_fields);	


$field_values="
	(NULL, 'none', 'none', '100', '400', 'none'),
	(NULL, 'default', 'default', '100', '400', 'edit_area')
";
LEPTON_handle::insert_values('mod_wysiwyg_admin', $field_values);


//	Additonal queries to avoid db-conflicts if the install.php is reloaded by the backend-adminstration.
$database->simple_query("DELETE FROM ".TABLE_PREFIX."sections WHERE section_id = -1 AND page_id =-120 "); 
$database->simple_query("DELETE FROM ".TABLE_PREFIX."mod_wysiwyg WHERE section_id = -1 AND page_id =-120 "); 

$fields = array(
	'page_id'	=> '-120',
	'section_id'=> '-1',
	'position'	=> '1',
	'module'	=> 'wysiwyg'
);
				
$database->build_and_execute(
	'INSERT',
	TABLE_PREFIX.'sections',
	$fields
);


$fields = array(
	'page_id'	=> '-120',
	'section_id'=> '-1',
	'content'	=> '<p><b>Berthold\'s</b> quick brown fox jumps over the lazy dog and feels as if he were in the seventh heaven of typography.</p>',
	'text'	=> 'Berthold\'s quick brown fox jumps over the lazy dog and feels as if he were in the seventh heaven of typography.',
	'content_comment'=> '',
	'working_content'=> '',
	'working_content_comment'=> ''
);
				
$database->build_and_execute(
	'INSERT',
	TABLE_PREFIX.'mod_wysiwyg',
	$fields
);

/**
 *	Looking for editors
 *
 */
$all = array();
$database->execute_query(
	"SELECT `directory` from `".TABLE_PREFIX."addons` where `function`='wysiwyg'",
	true,	
	$all,
	true
);

foreach($all as $ref) {
	$lookup = dirname(__FILE__)."/../".$ref['directory']."/class.editorinfo.php";
	if (file_exists($lookup)) {
		require_once($lookup);
		eval("\$editor_info = new editorinfo_".strtoupper($ref['directory'])."();");
		$editor_info->wysiwyg_admin_init( $database );
			
		unset( $editor_info );
	}
}
?>