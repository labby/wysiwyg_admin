### Wysiwyg Admin
=========

LEPTON Admintool to handle and manage settings for wysiwyg editors.


#### Requirements

* [LEPTON CMS][1], Version see precheck.php
 

#### Installation

* download latest [tool.zip][2] installation archive
* in CMS backend select the file from "Add-ons" -> "Modules" -> "Install module"

#### Notice

After installing addon go to admintools, enter tool and start to work. <br />
For further details please see [LEPTON forum][3].


[1]: https://lepton-cms.org "LEPTON CMS"
[2]: http://www.lepton-cms.com/lepador/admintools/wysiwyg_admin.php
[3]: https://forum.lepton-cms.org