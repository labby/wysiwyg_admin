<?php

/**
 *	@module			wysiwyg Admin
 *	@version		please see info.php of this module
 *	@author			Dietrich Roland Pehlke
 *	@copyright		2010-2020 Dietrich Roland Pehlke
 *	@link			https://lepton-cms.org
 *	@license		GNU General Public License
 *	@license_terms	please see info.php of this module
 *	@platform		please see info.php of this module
 *
 */

 // include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {
	include(LEPTON_PATH.'/framework/class.secure.php');
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) {
		include($root.'/framework/class.secure.php');
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 21:27, 25-06-2020
 * translated from..: EN
 * translated to....: IT
 * translated using.: www.DeepL.com/Translator
 * ==============================================
 */

$MOD_WYSIWYG_ADMIN	= array(
	"HEIGHT"				=> "Altezza dell'editor",
	"LEGEND1"				=> "in '%'. Non lasciare questi campi in bianco!",
	"LEGEND2"				=> "in 'px'. Non lasciare questi campi in bianco!",
	"PREVIEW"				=> "Anteprima",
	"SKINS"					=> "Selezionare la pelle",
	"TOOL"					=> "Barra degli strumenti dell'editor",
	"WIDTH"					=> "Larghezza dell'editor",
	"SAVE_OK"				=> "Le impostazioni vengono salvate.",
	"SAVE_FAILED"			=> "Si è verificato un errore durante il salvataggio.<br />Provare di nuovo.<br /><br />Se l'errore si verifica di nuovo, si prega di segnalare il problema <a href='https://forum.lepton-cms.org' target='_blank'>[qui]</a>.",
	"SAVE_FAIL_DRIVER"		=> "ATTENZIONE: uso di driver obsoleti! <br />Per favore aggiungete un class.editorinfo.php all'attuale wysiwyg-editor!"
);

?>
